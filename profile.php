<?php
require_once 'vendor/autoload.php';

use App\Core\DB;
use App\Helpers\Auth;
use App\Helpers\Session;
use App\Helpers\Redirect;

if (!Auth::isLogged()) {
    Redirect::to('index.php');
}

$db = new DB();
$id = Session::get('user_id');
$user = $db->first('SELECT * FROM users WHERE id=?', [$id]);

require_once 'header.php';
?>
    <div class="wrapper">
        <div class="container">
            <div class="media">
                <img src="<?php echo (empty($user['image'])) ? 'https://via.placeholder.com/150"' : $user['image'] ?>" class="align-self-start mr-3" alt="..." width="150" height="150">

                <div class="media-body">
                    <?php if (Session::has('success')): ?>
                        <div class="alert alert-success" role="alert">
                            <?= Session::pull('success') ?>
                        </div>
                    <?php elseif (Session::has('errors')): ?>
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <?php
                                    $errors = Session::pull('errors');
                                    foreach ($errors as $error):
                                ?>
                                        <li>
                                            <?= $error ?>
                                        </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <form action="update.php" id="user-form" method="POST" enctype="multipart/form-data" novalidate>
                        <input name="user_id" type="hidden" value="<?= $id; ?>">
                        <div class="form-row">
                            <div class="form-group col-md-6 required">
                                <label for="login">Login</label>
                                <input value="<?= $user['login'] ?>" name="login" type="text" class="form-control" id="login" minlength="5" maxlength="15" required disabled>
                            </div>
                            <div class="form-group col-md-6 required">
                                <label for="email">Email address</label>
                                <input value="<?= $user['email'] ?>" name="email" type="email" class="form-control" id="email" disabled>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6 required">
                                <label for="name">Name</label>
                                <input value="<?= $user['name'] ?>" name="name" type="text" class="form-control" id="name" minlength="3" maxlength="15" required>
                            </div>
                            <div class="form-group col-md-6 required">
                                <label for="surname">Surname</label>
                                <input value="<?= $user['surname'] ?>" name="surname" type="text" class="form-control" id="surname" minlength="3" maxlength="15" required>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label for="birth-date">Birth date</label>
                            <input value="<?= $user['date'] ?>" name="date" type="date" class="form-control" id="birth-date" required>
                        </div>
                        <div class="custom-file mb-3 mt-2">
                            <label class="custom-file-label" for="image">Upload image</label>
                            <input name="image" type="file" class="custom-file-input" id="image">
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php
require_once 'footer.php';