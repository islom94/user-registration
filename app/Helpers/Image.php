<?php


namespace App\Helpers;


class Image
{
    /**
     * Upload image to shown directory
     * @param $file
     * @param $directory
     * @return array
     */
    public static function upload($file, $directory)
    {

        if ($file["error"] != 0 || $file["size"] <= 0) {
            return self::error('File is empty');
        }

        $check = @getimagesize($file["tmp_name"]);

        if ($check === false) {
            return self::error('File is not image');
        }

        $ext = pathinfo(basename($file["name"]), PATHINFO_EXTENSION);

        if (!in_array($ext, ['gif', 'jpg', 'png', 'jpeg'])) {
            return self::error('Allowed upload only gif, jpg or png');
        }

        $fileMD5 = md5_file($file["tmp_name"]);
        $fullDir = $_SERVER["DOCUMENT_ROOT"] . "/" . $directory;

        if (!file_exists($fullDir)) {
            mkdir($fullDir);
            chmod($fullDir, 0777);
        }

        $fileName = $fileMD5 . "." . pathinfo(basename($file["name"]), PATHINFO_EXTENSION);
        $fullFile = $fullDir .DIRECTORY_SEPARATOR. $fileName;

        if (file_exists($fullFile)) {
            unlink($fullFile);
        }

        if (!move_uploaded_file($file["tmp_name"], $fullFile)) {
            return self::error('File is not uploaded');
        }

        return [
            "upload"    => true,
            "name"      => $fileName,
            "directory" => $directory,
            "full_path" => $fullFile
        ];
    }

    private static function error($msg) {
        return ['error' => $msg];
    }
}