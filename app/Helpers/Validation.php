<?php

namespace App\Helpers;

class Validation
{

    public static function errorMessage($rule)
    {
        switch ($rule) {
            case 'email':
                return 'Entered input is not email';
            case 'required':
                return 'This field is required for fill';
            default:
                return 'Error';
        }
    }

    public static function isEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public static function required($text)
    {
        $text = trim($text);
        return !empty($text);
    }

    public static function min($input, $length)
    {
        return strlen($input) >= $length;
    }

    public static function max($input, $length)
    {
        return strlen($input) <= $length;
    }

    public static function validate($rules)
    {
        if (!is_array($rules)) {
            return false;
        }

        $errors = [];
        foreach ($rules as $field => $rule) {
            foreach (explode(',', $rule) as $item) {
                $value = $_POST[$field] ?? '';
                if (!self::checkRule($value, $item)) {
                    $errors[] = self::errorMessage($item);
                }
            }
        }
        return empty($errors) ? true : $errors;
    }

    public static function checkRule($input, $rule)
    {
        if ($rule == 'required') {
            return self::required($input);
        } elseif ($rule == 'email') {
            return self::isEmail($input);
        } elseif (strpos($rule, 'min') !== false) {
            $value = trim(substr($rule, strpos($rule, ':') + 1));
            return self::min($input, $value);
        } elseif (strpos($rule, 'max') !== false) {
            $value = trim(substr($rule, strpos($rule, ':') + 1));
            return self::max($input, $value);
        }
        return true;
    }
}