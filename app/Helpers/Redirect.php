<?php

namespace App\Helpers;

class Redirect {
    public static function to($pathToFile)
    {
        if ($pathToFile[0] != '/') {
            $pathToFile = '/'.$pathToFile;
        }
        header('Location: http://localhost'.$pathToFile);
        exit(0);
    }
} 