<?php

namespace App\Helpers;

use App\Core\DB;
use App\Helpers\Session;

class Auth {

    public static function isLogged()
    {
        Session::start();
        return isset($_SESSION['login']);
    }

    public static function login($email, $password)
    {
        if (empty($email) || empty($password)) {
            return false;
        }
        $db   = new DB();
        $user = $db->first('SELECT * FROM users WHERE email=?', [$email]);

        if (!empty($user)) {
            if (password_verify($password, $user['password'])) {
                Session::set('login', $user['login']);
                Session::set('user_id', $user['id']);
                return true;
            }
        }
        return false;
    }
} 