<?php

namespace App\Helpers;

class Session
{

    public static function start()
    {
        $status = session_status();

        if ($status == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public static function success($msg)
    {
        if (empty($msg)) {
            return;
        }
        self::start();
        $_SESSION['success'] = $msg;
    }

    public static function set($key, $value)
    {
        self::start();
        $_SESSION[$key] = $value;
    }

    public static function has($key)
    {
        return !empty($_SESSION[$key]);
    }

    public static function get($key, $default = '')
    {
        return self::has($key) ? $_SESSION[$key] : $default;
    }

    public static function pull($key)
    {
        $value = self::get($key);
        self::remove($key);
        return $value;
    }

    public static function remove($key)
    {
        if (self::has($key)) {
            unset($_SESSION[$key]);
        }
    }
} 