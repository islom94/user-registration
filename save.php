<?php
require_once 'config/db.php';
require_once 'vendor/autoload.php';

use App\Helpers\Session;
use App\Helpers\Redirect;
use App\Helpers\Auth;
use App\Core\DB;

$db = new DB();

$columns = ['login', 'email', 'name', 'surname', 'date', 'password', 'image'];
$data = [];

foreach ($columns as $column) {
    $data[$column] = $_POST[$column] ?? '';
}
$data['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);

$db->insert('users', $columns, $data);

Session::success('You have registered successfully');
Auth::login($data['email'], $data['password']);
Redirect::to('index.php');
