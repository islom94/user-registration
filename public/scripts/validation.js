(function ($) {
    /*
        Plugin for validation form returns true if form inputs is valid
        else returns array of objects which key is input name, value is error message
     */
    $.fn.validate = function () {

        function validateEmail(email) {
            let re = /\S+@\S+\.\S+/;
            return re.test(email);
        }

        var errors = {};
        var valid = true;

        this.find('input').each(function () {

            if (!this.hasAttribute('name')) {
                return;
            }

            let value = $(this).val().trim();
            let name  = $(this).attr('name');

            if (name.length === 0) {
                return;
            }

            if (this.hasAttribute('required') && value.length === 0) {
                errors[name] = 'This field is required for fill';
                valid = false;
            } else if (this.hasAttribute('minlength') && $(this).attr('minlength') > value.length) {
                errors[name] = 'This field must contain at least ' + $(this).attr('minlength') + ' characters';
                valid = false;
            } else if (this.hasAttribute('maxlength') && $(this).attr('maxlength') < value.length) {
                errors[name] = 'This field must contain at most ' + $(this).attr('maxlength') + ' characters';
                valid = false;
            } else if ($(this).attr('type') == 'email' && !validateEmail(value)) {
                errors[name] = 'Please enter correct email';
                valid = false;
            }

        });

        if (valid === false) {
            return errors;
        }

        return true;
    };
})(jQuery);