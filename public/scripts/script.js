$(document).ready(function () {
    $('#user-form').on('submit', function (e) {
        var valid = $(this).validate();
        if (valid !== true) {
            e.preventDefault();
            $(this).find('input').each(function () {
                let $input = $(this);
                let name = $input.attr('name');

                if ($input.hasClass('is-valid')) {
                    $input.removeClass('is-valid');
                }
                if ($input.hasClass('is-invalid')) {
                    $input.removeClass('is-invalid');
                }

                if (!valid.hasOwnProperty($input.attr('name'))) {
                    $input.addClass('is-valid');
                    return;
                }

                let $formGroup = $input.closest('.form-group');

                $input.addClass('is-invalid');

                if ($formGroup.find('.invalid-feedback').length === 0) {
                    $formGroup.append('<div class="invalid-feedback"></div>');
                }

                $formGroup.find('.invalid-feedback').text(valid[name]);
            });
        }
    });
});