<?php
    include_once 'header.php';
    require_once 'vendor/autoload.php';

use App\Helpers\Session;
?>
<div class="wrapper">
    <div class="container">
        <?php
        if(Session::has('success')):
            ?>
            <div class="alert alert-success" role="alert">
                <?= Session::pull('success') ?>
            </div>
        <?php
        endif;
        ?>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores facere illo impedit itaque perferendis quisquam rerum totam ut velit vitae.</p>
    </div>
</div>

<?php
    include_once 'footer.php';
