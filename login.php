<?php
require_once 'vendor/autoload.php';

use App\Helpers\Auth;
use App\Helpers\Redirect;
use App\Helpers\Session;

$error = '';

if (Auth::isLogged()) {
    Redirect::to('index.php');
}
if (isset($_POST['email']) && isset($_POST['password'])) {
    $email    = $_POST['email'];
    $password = $_POST['password'];

    if (Auth::login($email, $password)) {
        Session::success('You have logined sucessfully');
        Redirect::to('index.php');
    } else {
        $error = 'Email or password entered incorrectly';
    }
}

require_once 'header.php';
?>

<div class="wrapper">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <?php if (!empty($error)): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $error ?>
                    </div>
                <?php endif; ?>
                <div class="card">
                    <div class="card-header">Login</div>
                    <div class="card-body">
                        <form id="user-form" method="POST" enctype="multipart/form-data" novalidate>
                            <div class="form-group required">
                                <label for="email">Email address</label>
                                <input name="email" type="email" class="form-control" id="email">
                            </div>
                            <div class="form-group required">
                                <label for="password">Password</label>
                                <input name="password" type="password" class="form-control" id="password" required>
                            </div>
                            <button type="submit" class="btn btn-primary float-right">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once 'footer.php';
