<?php
    require_once 'vendor/autoload.php';

    use App\Helpers\Auth;
?>
<!DOCTYPE html>
<html>
<head>
    <title>Registration project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="public/styles/bootstrap.min.css">
    <link rel="stylesheet" href="public/styles/style.css">
    <link rel="stylesheet" href="public/styles/fontawesome.min.css">
    <link rel="stylesheet" href="public/styles/solid.min.css">
    <link rel="stylesheet" href="public/styles/brands.min.css">

    <script src="public/scripts/jquery-3.4.1.min.js"></script>
    <script src="public/scripts/bootstrap.min.js"></script>
    <script src="public/scripts/validation.js"></script>
    <script src="public/scripts/script.js"></script>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbar-content"
                    aria-controls="navbar-content" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbar-content">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <?php if (Auth::isLogged()): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/profile.php">Profile</a>
                        </li>
                    <?php else: ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/registration.php">Register</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/login.php">Login</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>
</header>


