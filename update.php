<?php
require_once 'vendor/autoload.php';

use App\Core\DB;
use App\Helpers\Image;
use App\Helpers\Auth;
use App\Helpers\Redirect;
use App\Helpers\Validation;
use App\Helpers\Session;

if (!Auth::isLogged() || empty($_POST['user_id'])) {
    Redirect::to('index.php');
}
$db = new DB();

$image = null;
$errors = [];

if (!empty($_FILES['image'])) {
    $upload = Image::upload($_FILES['image'], 'public/uploads/images');
    if (isset($upload['error'])) {
        $errors[] = $upload['error'];
    } else {
        $image = $upload['directory'].'/'.$upload['name'];
    }
}

$rules = [
    'name'    => 'required,min:3,max:15',
    'surname' => 'required,min:3,max:15',
    'date'    => 'required'
];

$validate = Validation::validate($rules);

if ($validate === true && empty($errors)) {
    Session::success('Sucessfully updated');
    $sql = 'UPDATE users SET name=?, surname=?, date=?, image=? WHERE id=?';
    $id = Session::get('user_id');
    $db->query($sql, [$_POST['name'], $_POST['surname'], $_POST['date'], $image, $id]);
} else {
    if ($validate === true) {
        $validate = [];
    }
    $errors = array_merge($errors, $validate);
    Session::set('errors', $errors);
}
Redirect::to('profile.php');
