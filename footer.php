<footer class="bg-light">
    <div class="row">
        <div class="icons mx-auto my-3">
            <!-- Facebook -->
            <a class="icon fb-ic" href="https://www.facebook.com" target="_blank">
                <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>
            <!-- Twitter -->
            <a class="icon tw-ic" href="https://twitter.com" target="_blank">
                <i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>
            <!--Linkedin -->
            <a class="icon li-ic" href="https://www.linkedin.com" target="_blank">
                <i class="fab fa-linkedin-in fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>
            <!--Instagram-->
            <a class="icon ins-ic" href="https://www.instagram.com" target="_blank">
                <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>
            <!--Pinterest-->
            <a class="icon pin-ic" href="https://www.pinterest.com" target="_blank">
                <i class="fab fa-pinterest fa-lg white-text fa-2x"> </i>
            </a>
        </div>
    </div>
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
    </div>
</footer>
</body>
</html>
