<?php
require_once 'vendor/autoload.php';
use App\Helpers\Session;

include_once 'header.php';
?>

<div class="wrapper">
    <div class="container">
        <form action="save.php" id="user-form" method="POST" enctype="multipart/form-data" novalidate>
            <?php if (Session::has('errors')): ?>
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <?php
                        $errors = Session::pull('errors');
                        foreach ($errors as $error):
                        ?>
                        <li>
                            <?= $error ?>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
            <div class="form-row">
                <div class="form-group col-md-6 required">
                    <label for="login">Login</label>
                    <input name="login" type="text" class="form-control" id="login" minlength="5" maxlength="15" required>
                </div>
                <div class="form-group col-md-6 required">
                    <label for="email">Email address</label>
                    <input name="email" type="email" class="form-control" id="email">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6 required">
                    <label for="name">Name</label>
                    <input name="name" type="text" class="form-control" id="name" minlength="3" maxlength="15" required>
                </div>
                <div class="form-group col-md-6 required">
                    <label for="surname">Surname</label>
                    <input name="surname" type="text" class="form-control" id="surname" minlength="3" maxlength="15" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6 required">
                    <label for="birth-date">Birth date</label>
                    <input name="date" type="date" class="form-control" id="birth-date" required>
                </div>
                <div class="form-group col-md-6 required">
                    <label for="password">Password</label>
                    <input name="password" type="password" class="form-control" id="password" minlength="6" maxlength="20" required>
                </div>
            </div>
            <div class="custom-file mb-3 mt-2">
                <label class="custom-file-label" for="image">Upload image</label>
                <input name="image" type="file" class="custom-file-input" id="image">
            </div>
            <button type="submit" class="btn btn-primary float-right">Submit</button>
        </form>
    </div>
</div>

<?php
    include_once 'footer.php';